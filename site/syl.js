//////////////////
/// PARAMETERS ///
//////////////////

const SylParams = {

  // Diagram characters
  displayChars: {
    '0': '⏺',
    '1': '●',
    's': '~',
    'v': '︲',
    'd': '⁄',
    'h': '⎯',
    '-': '‧'
  },

  // Rhyme colors
  rColors: [
    '#FABC3C',
    '#DD5510',
    '#006992',
    '#4AA04D',
    '#2C9187',
    '#EAEFBD',
    '#EA9010',
    '#89608E',
    '#E3655B',
    '#11B5E4'
  ],

  // Starting poem
  defaultPoem:'Natural rhythm: we speak staccato\nEnunciate each word with vigor\nFor language-game aficionados\nSyllable-stress deserves some rigor',

  // Hob interval
  hobInterval: 50

};



///////////////
/// DIAGRAM ///
///////////////

class Diagram {
  e;
  #displayChars; #rColors;

  constructor(displayChars_, rColors_) {
    this.e = $('<div class="diagram"></div>');
    this.#displayChars = displayChars_;
    this.#rColors = rColors_;
    return this;
  }

  render(lexed) {
    let rm = this.#makeRhymeMap((lexed.replace(/{.}/g, '').match(/\w+/g) || []).map((w) => w.trim()));
    let diagram = lexed.split(/\s+/).map((x) => {
      if (x == '{s}') return `<div class="sectionSep">${this.#displayChars['s']}</div>`;
      if (x == '{n}') return '<br class="lineSep">';
      if (x.match(/{.}/)) return `<div class="symbol">${this.#displayChars[x[1]]}</div>`;
      return this.#renderWord(x, rm);
    }).join(' ');
    this.e.empty();
    this.e.html(diagram);
  }

  #renderWord(word, rhymeMap) {
    return (rhymeMap[word])
      ? `<span class="word" style="color: ${this.#rColors[rhymeMap[word]]}">${this.#syllables(word)}</span>`
      : `<span class="word">${this.#syllables(word)}</span>`;
    
  }

  // Check if two words are rhymes
  // (also allows partial-rhymes if either is a plural ending in 's' and the singular is a rhyme)
  #isRhyme(a, b) {
    if (RiTa.isRhyme(a, b)) return true;
    let isPlural = (x) => x == RiTa.pluralize(x);
    if (a.slice(-1) == 's' && isPlural(a) && RiTa.isRhyme(a.slice(0, -1), b)) return true;
    if (b.slice(-1) == 's' && isPlural(b) && RiTa.isRhyme(a, b.slice(0, -1))) return true;
    return false;
  }

  #addRhyme(rGroups, word) {
    let foundRhyme = false;
    for (let i = 0; i < rGroups.length; i++) {
      if (rGroups[i].includes(word)) return;
      if (this.#isRhyme(rGroups[i][0], word)) { rGroups[i].push(word); return }
    }
    rGroups.push([word]);
  }

  #makeRhymeMap(words) {
    let rGroups = [];
    let n = 0;
    let rm = {};
    words.map((word) => this.#addRhyme(rGroups, word));
    rGroups.map((group) => {
      if (group.length > 1) { group.map((word) => rm[word] = n); n++; }
    });
    return rm;
  }

  #syllables(word) {
    return RiTa.stresses(word, {silent: true})
      .replace(/[^01]/g, '')
      .replace(/0/g, this.#displayChars['0'])
      .replace(/1/g, this.#displayChars['1']);
  }
}



/////////////
/// STATS ///
/////////////

class Stats {
  e;
  #sectionCount; #lineCount; #wordCount; #characterCount;

  constructor() {
    this.e = $('<div class="stats"></div>');
    this.#sectionCount = $('<span class="sectionCount" title="Section Count"></span>');
    this.#lineCount = $('<span class="lineCount" title="Line Count"></span>');
    this.#wordCount = $('<span class="wordCount" title="Word Count"></span>');
    this.#characterCount = $('<span class="characterCount" title="Character Count"></span>');
    this.e.append(
      this.#sectionCount,
      $('<span class="stats-sep">&#47;</span>'),
      this.#lineCount,
      $('<span class="stats-sep">&#47;</span>'),
      this.#wordCount,
      $('<span class="stats-sep">&#47;</span>'),
      this.#characterCount,
    );
    return this;
  }

  update(sc, lc, wc, cc) {
    this.#sectionCount.text(sc);
    this.#lineCount.text(lc);
    this.#wordCount.text(wc);
    this.#characterCount.text(cc);
  }
}



///////////////////
/// INPUT FIELD ///
///////////////////

class InputField {
  e;
  #diagram; #stats; #lexed;

  constructor(diagram_, stats_, defaultContents=' ', hobInterval=500) {
    this.e = $('<pre contenteditable spellcheck="false" class="fserif input-field"></pre>');
    this.#diagram = diagram_;
    this.#stats = stats_;
    this.#lexed = null;
    this.e.text(defaultContents);
    this.#hob(hobInterval);
    this.#update();
    this.#initListeners();
    return this;
  }

  #update() {
    this.#lexed = null;
    this.#updateStats();
    this.#updateDiagram();
  }

  #updateStats() { this.#stats.update(this.#sectionCount(), this.#lineCount(), this.#wordCount(), this.#characterCount()); }

  #updateDiagram() { this.#diagram.render(this.#lex()); }

  #sectionCount() { return (this.#lex().match(/{s}/g) || []).length + 1; }

  #lineCount() { return (this.#lex().match(/{n}/g) || []).length + 1; }

  #wordCount() { return (this.#lex().replace(/{.}/g, '').match(/\w+/g) || []).length; }

  #lex() {
    if (this.#lexed == null) this.#lexed = this.e[0].innerText
      .trim() // remove pre/post-fix whitespace
      .toLowerCase() // convert input to all lower-case
      .replace(/[\{\}]/g, '') // remove any curly braces
      .replace(/\n\s*\n/g, ' {s} ') // replace two (or more) adjacent newlines with a section seperator
      .replace(/[^\w]*\n[^\w]*/g, ' {n} ') // replace all other newlines with a line seperator
      .replace(/[.!?]/g, ' {v} ') // replace sentence-end punctuation with a clause seperator
      .replace(/[:,…]/g, ' {d} ') // replace conjunction punctuation with a clause seperator
      .replace(/[;—/]/g, ' {h} ') // replace concatenation punctuation with a clause seperator
      .replace(/\s+\d+\s+/g, (n) => ' ' + this.#numToWords(n.trim()) + ' ') // convert numbers to words
      .replace(/[^a-zA-z0-9-\s{}\[\]]/g, ''); // remove all other characters
    return this.#lexed;
  }

  // Convert number < 9999 to compound word
  #numToWords(n) {
    if (n == '0') return 'zero';
    n = n.replace(/^0+/g, '');
    if (n == '') return '';
    let N = n.split('').map((d) => parseInt(d, 10));
    let ones = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
    let teens = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
    let tens = ['twenty', 'thirty', 'fourty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
    if (n.length == 1) return ones[N[0]];
    if (n.length == 2 && n[0] == '1') return teens[N[1]];
    if (n.length == 2 && n[1] == '0') return tens[N[0]-2];
    if (n.length == 2) return tens[N[0]-2] + ones[N[1]];
    if (n.length == 3) return ones[N[0]] + 'hundred' + this.numToWords(n.slice(1));
    if (n.length == 4 && n[2] == '0') return this.numToWords(n.slice(0, 2)) + '-o-' + this.numToWords(n.slice(2));
    if (n.length == 4) return this.numToWords(n.slice(0, 2)) + '-' + this.numToWords(n.slice(2));
    return n;
  };

  #characterCount() { return this.e.text().length; }

  #hob(interval) {
    if (this.e.text() == '') this.e.text(' ');
    this.e.focus();
    setTimeout(() => this.#hob(interval), interval);
  }

  #initListeners() {
    this.e.on('input', () => this.#update());
  }
}
  


////////////////////
/// DATAMUSE API ///
////////////////////

class Datamuse {
  static async runQuery(mode, rawQuery) {
    let query = this.#formatQuery(rawQuery);
    let res = await fetch(`https://api.datamuse.com/words?${mode}=${query}`);
    let json = await res.json();
    return json.map((x) => x.word);
  }

  static #formatQuery(rawQuery) {
    return rawQuery.trim().toLowerCase().replace(/\s+/g, '+');
  }
}



/////////////////////////
/// CLIPBOARD HELPERS ///
/////////////////////////

class Clipboard {
  #data;

  constructor() {
    // this is little work-around for browsers (like Firefox) which lack clipboard read access
    this.#data = (navigator.clipboard.readText == null) ? ' ' : null; 
  }

  async copy(str=null) {
    let to_copy = str || this.getSelected();
    await navigator.clipboard.writeText(to_copy);
    if (this.#data) this.#data = to_copy;
  }

  async paste() {
    let to_paste = this.#data || await navigator.clipboard.readText();
    let range = document.getSelection().getRangeAt(0);
    range.deleteContents();
    range.insertNode(document.createTextNode(to_paste));
  }

  async cut() {
    let range = document.getSelection().getRangeAt(0);
    if (range.collapsed) return;
    let to_cut = range.toString();
    range.deleteContents();
    await navigator.clipboard.writeText(to_cut);
    if (this.#data) this.#data = to_cut;
  }

  // Get the currently selected word
  getSelected() {
    let sel = document.getSelection();
    if (sel.type == 'Caret') {
      sel.modify('move', 'backward', 'word');
      sel.modify('extend', 'forward', 'word');
    }
    return sel.toString().trim();
  }
}



///////////////////////
/// QUERY SIDEPANEL ///
///////////////////////

class QueryPanel {
  e;
  #clipboard;
  #closeBtn; #type; #target; #results;

  constructor(clipboard_) {
    this.e = $('<div class="query hidden greyBG"></div>');
    this.#closeBtn = $('<div class="query-hide clickable">✕</div>');
    this.#type = $('<div class="query-type fullwidth"></div>');
    this.#target = $('<div class="query-target fserif fullwidth"></div>');
    this.#results = $('<div class="query-results fserif"></div>');
    this.e.append(this.#closeBtn, this.#type, this.#target, this.#results);
    this.#clipboard = clipboard_;
    this.#initListeners();
    return this;
  }

  show() { this.e.addClass('on'); }

  hide() { this.e.removeClass('on'); }

  similar(word) { this.#showResults('ml', 'Similar', word); }

  antonyms(word) { this.#showResults('rel_ant', 'Antonyms', word); }

  rhymes(word) { this.#showResults('rel_rhy', 'Rhymes', word); }

  nearRhymes(word) { this.#showResults('rel_nry', 'Near-Rhymes', word); }

  soundsLike(word) { this.#showResults('sl', 'Sounds-Like', word); }

  async #showResults(mode, modeName, word, sortFunc=null) {
    this.#type.text(modeName);
    this.#target.text(word);
    let queryResponse = await Datamuse.runQuery(mode, word);
    this.#results.empty();
    if (sortFunc) queryResponse = queryResponse.sort(sortFunc);
    queryResponse.map((result) => {
      let elem = $(`<div class="clickable">${result}</div>`);
      elem.on('click', () => this.#clipboard.copy(result));
      this.#results.append(elem);
    });
    this.show();
  }

  #initListeners() {
    // Query-sidepanel close button
    this.#closeBtn.on('click', () => this.hide());
  }
}



////////////////////
/// CONTEXT MENU ///
////////////////////

class ContextMenu {
  e;
  #input; #queryPanel; #clipboard;
  #copyBtn; #pasteBtn; #cutBtn;
  #similarBtn; #antonymsBtn; #rhymesBtn; #nearRhymesBtn; #soundsLikeBtn;

  constructor(input_, queryPanel_, clipboard_) {
    this.#copyBtn = $('<li class="copy-btn clickable">⧉ Copy</li>');
    this.#pasteBtn = $('<li class="paste-btn clickable"><span style="font-size: 10px;">📋</span> Paste</li>');
    this.#cutBtn = $('<li class="cut-btn clickable">✂ Cut</li>');
    this.#similarBtn = $('<li class="similar clickable">Similar</li>');
    this.#antonymsBtn = $('<li class="antonyms-btn clickable">Antonyms</li>');
    this.#rhymesBtn = $('<li class="rhymes-btn clickable">Rhymes</li>');
    this.#nearRhymesBtn = $('<li class="near-ryhmes-btn clickable">Near-Rhymes</li>');
    this.#soundsLikeBtn = $('<li class="sounds-like-btn clickable">Sounds-Like</li>');
    this.e = $('<nav class="context-menu hidden greyBG"></nav>').append(
      $('<ul class="clipboard-btns"></ul>').append(
        this.#copyBtn,
        this.#pasteBtn,
        this.#cutBtn
      ),
      $('<hr>'),
      $('<ul class="word-queries"></ul>').append(
        this.#similarBtn,
        this.#antonymsBtn,
        this.#rhymesBtn,
        this.#nearRhymesBtn,
        this.#soundsLikeBtn
      )
    );
    this.#input = input_;
    this.#queryPanel = queryPanel_;
    this.#clipboard = clipboard_;
    this.#initListeners();
    return this;
  }

  show(ev) { this.e.addClass('on'); this.e.offset({left: ev.pageX, top: ev.pageY}); this.#input.e.focus(); }

  hide() { this.e.removeClass('on'); this.#input.e.focus(); }

  #initListeners() {
    // Show context menu if input is right clicked
    this.#input.e.on('contextmenu', (ev) => { ev.preventDefault(); ev.stopPropagation(); this.show(ev); });
    
    // Clipboard buttons
    this.#copyBtn.on('click', () => { this.hide(); this.#clipboard.copy(); });
    this.#pasteBtn.on('click', () => { this.hide(); this.#clipboard.paste(); });
    this.#cutBtn.on('click', () => { this.hide(); this.#clipboard.cut(); });

    // Word buttons
    this.#similarBtn.on('click', () => { this.hide(); this.#queryPanel.similar(this.#clipboard.getSelected()); });
    this.#antonymsBtn.on('click', () => { this.hide(); this.#queryPanel.antonyms(this.#clipboard.getSelected()); });
    this.#rhymesBtn.on('click', () => { this.hide(); this.#queryPanel.rhymes(this.#clipboard.getSelected()); });
    this.#nearRhymesBtn.on('click', () => { this.hide(); this.#queryPanel.nearRhymes(this.#clipboard.getSelected()); });
    this.#soundsLikeBtn.on('click', () => { this.hide(); this.#queryPanel.soundsLike(this.#clipboard.getSelected()); });
  }
}



//////////
/// UI ///
//////////

class SylUI {
  e;
  #stats; #diagram; #arrow; #input; #clipboard; #queryPanel; #ctxMenu;
  
  constructor(parent, params) {
    this.e = $('<div class="syl-ui"></div>');
    this.#stats = new Stats();
    this.#diagram = new Diagram(params.displayChars, params.rColors);
    this.#arrow = $('<img class="ui-arrow" src="arrow.svg">');
    this.#input = new InputField(this.#diagram, this.#stats, params.defaultPoem, params.hobInterval);
    this.e.append(this.#input.e, this.#stats.e, this.#arrow, this.#diagram.e);
    
    this.#clipboard = new Clipboard();
    this.#queryPanel = new QueryPanel(this.#clipboard);
    this.#ctxMenu = new ContextMenu(this.#input, this.#queryPanel, this.#clipboard);
    parent.append(this.e, this.#queryPanel.e, this.#ctxMenu.e);
    
    this.#initListeners();
    return this;
  }

  #initListeners() {
    document.addEventListener('mousedown', (ev) => {
      // Don't clear text-selection when using context menu items or query sidepanel
      let clickInCtxMenu = this.#ctxMenu.e.get(0).contains(ev.target);
      let clickInQueryPanel = this.#queryPanel.e.get(0).contains(ev.target);
      if (clickInCtxMenu || clickInQueryPanel) ev.preventDefault();
      else this.#ctxMenu.hide(); // Otherwise make sure context menu is hidden
    });
  }
}